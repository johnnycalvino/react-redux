| Branch | Status |
|--------|--------|
| [Redux Firebase Branch](https://bitbucket.org/johnnycalvino/react-redux/src/ff8f3e2f6d164a51bd155d420b8ffaa120605296/?at=feature%2Freact-redux-firebase) | [ ![Codeship Status for drewkitch/codeship-pro-observations](https://app.codeship.com/projects/35f69bf0-ab27-0136-1cc0-5a17bf1d0a96/status?branch=feature/react-redux-firebase)](https://app.codeship.com/projects/309260)  |
| [Development Branch](https://bitbucket.org/johnnycalvino/react-redux/src/develop/) | [ ![Codeship Status for drewkitch/codeship-pro-observations](https://app.codeship.com/projects/35f69bf0-ab27-0136-1cc0-5a17bf1d0a96/status?branch=develop)](https://app.codeship.com/projects/309260)|



| Deploy | Status |
|--------|--------|
| [Heroku](https://react-boilerplat.herokuapp.com/) | [ ![heroky deploy](https://heroku-badge.herokuapp.com/?app=react-boilerplat)](https://dashboard.heroku.com/apps/react-boilerplat/deploy/heroku-git)  |