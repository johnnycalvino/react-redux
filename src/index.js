import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import { Route, Switch } from 'react-router'; // react-router v4
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { Helmet } from 'react-helmet';

import * as serviceWorker from './config/serviceWorker';
import indexRoutes from './routes';
import { configureStore, history, currentLanguage } from './config/configureStore';
import i18n from './config/i18n';

const store = configureStore();

// Init whith default languuage == es
i18n.init({ lng: currentLanguage('/en') });

ReactDOM.render(
  <I18nextProvider i18n={i18n}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div>
          <Helmet
            onChangeClientState={(newState, addedTags, removedTags) => {
              console.log({
                newState,
                addedTags,
                removedTags,
              });
            }}
          >
            <html lang={currentLanguage()} amp />
            <meta charSet="utf-8" />
            <title>My App</title>
          </Helmet>
          <Switch>
            {indexRoutes.map(
              (prop, key) => (<Route path={prop.path} key={key} component={prop.component} />),
            )}
          </Switch>
        </div>
      </ConnectedRouter>
    </Provider>
  </I18nextProvider>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitf8alls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
