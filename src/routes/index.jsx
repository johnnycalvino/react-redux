import App from '../views/app/App';

const indexRoutes = [
  { path: `${process.env.PUBLIC_URL}/`, name: 'App', component: App },
];

export default indexRoutes;
