const proxy = require('http-proxy-middleware');

const routes = [
  '/p', ,
];

module.exports = function (app) {
  app.use(proxy(routes, {
    target: 'https://jsonplaceholder.typicode.com',
    changeOrigin: true, // for vhosted sites, changes host header to match to target's host
    logLevel: 'debug',
    cookieDomainRewrite: {
      localhost: 'jsonplaceholder.typicode.com',
      '*': '',
    },
    pathRewrite: {
      '^/p': '/posts',
    },
  }));
};
