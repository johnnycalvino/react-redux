import { createStore, compose, applyMiddleware } from 'redux';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import rootReducer from '../store/reducers';
import fbConfig from './firebaseConfig';

// Set the current language with the URL
export function currentLanguage(defaultLang = 'en') {
  const path = window.location.pathname.split('/');
  if (path[1] === 'en') {
    return path[1];
  } if (path[1] === 'es') {
    return path[1];
  }
  return defaultLang;
}

export const history = createBrowserHistory({ basename: currentLanguage() });

export function configureStore(initialState) {
  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    composeEnhancers(
      applyMiddleware(
        thunk.withExtraArgument({ getFirebase, getFirestore }), routerMiddleware(history),
      ),
      reactReduxFirebase(fbConfig, { userProfile: 'users', useFirestoreForProfile: true, attachAuthIsReady: true }),
      reduxFirestore(fbConfig), // redux bindings for firestore
    ),
  );

  return store;
}
