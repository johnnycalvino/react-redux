/**
 * Capitalize strings "code is my life"
 * To codeIsMyLife
 * @param {string} text
 */
export const camelize = text => text.replace(/(?:^\w|[A-Z]|\b\w)/g, 
  (letter, index) => index === 0 ? letter.toLowerCase() :
  letter.toUpperCase()).replace(/\s+/g, '');


/*
 * To convert object in a flat object
 * { a: b: { a: 1, b: 2} }
 * to  { a.b.a: 1, a.b.b: 2 }
 * @param {String} prefix
 * @param {Object} source
 * @param {Object} dest
 * TODO traslate to PureFunction
 */
export const flat = (prefix, source, dest) => {
  Object.keys(source).forEach((k) => {
    const value = source[k];
    const pref = prefix ? `${prefix}.${k}` : k;

    if (value && typeof value === 'object') {
      flat(pref, value, dest);
    } else {
      const p = camelize(pref.replace(/([.])/g, ' '));
      dest[p] = value;
    }
  });
  return dest;
};
