import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Replace this with your own config details
var config = {
  apiKey: "AIzaSyDuwSpOLgVV-llBAi2oqxMJMiFkT_544Mc",
  authDomain: "planb-2018.firebaseapp.com",
  databaseURL: "https://planb-2018.firebaseio.com",
  projectId: "planb-2018",
  storageBucket: "planb-2018.appspot.com",
  messagingSenderId: "996820111367"
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase 