import React from 'react';
import PropTypes from 'prop-types';
import './button.cmp.scss';

const Button = (props) => {
  const {
    type,
    classes,
    disabled,
    onPress,
    extraprops,
    children,
  } = props;
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={onPress}
      className={['btn', classes].join(' ')}
      {...extraprops}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  type: 'button',
  classes: '',
  disabled: false,
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  classes: PropTypes.string,
};

export default Button;
