import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import './App.scss';
import ExampleActions from '../../store/actions/app.action';
import i18n from '../../config/i18n';
import Button from '../../components/button/button.cmp';

class App extends Component {
  requestBDtoRedux = () => {
    const { createProject } = this.props;
    createProject({
      title: 'Desde App',
      content: 'Content',
      authorFirstName: 'admin',
      authorLastName: 'admin',
      authorId: 'lastID',
      createdAt: new Date(),
    });
  }

  requestToRedux = () => {
    const { request } = this.props;
    request({
      url: '/p',
      method: 'get',
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>{i18n.t('request')}</p>
          <Button onPress={this.requestToRedux}>
            {i18n.t('requestToRedux')}
          </Button>
          <Button onPress={this.requestBDtoRedux} classes="danger">
            {i18n.t('requestToCreateDocumentFirebase')}
          </Button>
        </header>
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state });
const mapDispatchToProps = dispatch => ({
  request: (data) => {
    dispatch(ExampleActions.request(data));
  },
  goTo: (url) => {
    dispatch(push(url));
  },
  createProject: (project) => {
    dispatch(ExampleActions.addColection(project));
  },
});

const Connect = connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

export default Connect;
