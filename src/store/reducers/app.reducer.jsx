import ExampleActions from '../actions/app.action';

function initialState() {
  return {
    err: null,
    loader: 0,
    get: {},
    post: {},
    data: [],
  };
}

const exampleReducer = (state = initialState(), action) => {
  const newState = state;
  switch (action.type) {
    case ExampleActions.UPDATE_ERROR:
      return { ...state, err: action.payload };

    case ExampleActions.INCREMENT_LOADER:
      return { ...state, loader: state.loader + 1 };

    case ExampleActions.DECREMENT_LOADER:
      return { ...state, loader: state.loader - 1 };

    case ExampleActions.RESET_LOADER:
      newState.loader = 0;
      return { ...state, loader: newState.loader };

    case ExampleActions.GET_REQUEST:
      return { ...state, get: action.payload };

    case ExampleActions.POST_REQUEST:
      return { ...state, post: action.payload };

    case ExampleActions.CREATE_COLECTION:
      return { ...state, data: action.payload };

    default:
      return state;
  }
};

export default exampleReducer;
