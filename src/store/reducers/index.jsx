import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';
import appReducer from './app.reducer';

const rootReducer = combineReducers({
  example: appReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
});

export default rootReducer;
