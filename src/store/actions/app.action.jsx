import axios from 'axios';
import { push } from 'react-router-redux';
import { flat } from '../../config/utils';

const tag = '[Example] ';

export default class ExampleActions {
  // Action types
  static UPDATE_ERROR = `${tag} UPDATE_ERROR`;

  static INCREMENT_LOADER = `${tag} INCREMENT_LOADER`;

  static DECREMENT_LOADER = `${tag} DECREMENT_LOADER`;

  static RESET_LOADER = `${tag} RESET_LOADER`;

  static GET_REQUEST = `${tag} GET_REQUEST`;

  static POST_REQUEST = `${tag} POST_REQUEST`;

  static PATCH_REQUEST = `${tag} PATCH_REQUEST`;

  static UPDATE_REQUEST = `${tag} UPDATE_REQUEST`;

  static CREATE_COLECTION = `${tag} CREATE_COLECTION`;

  // Action creators default
  // Used to set All Errors on Redux
  static updateError = payload => ({ type: ExampleActions.UPDATE_ERROR, payload });

  // To set a loater automatic
  static incrementLoader = () => ({ type: ExampleActions.INCREMENT_LOADER });

  static decrementLoader = () => ({ type: ExampleActions.DECREMENT_LOADER });

  static resetLoader = () => ({ type: ExampleActions.RESET_LOADER });

  // Action set asyncData in redux example
  static request = paramsToRequest => (dispatch) => {
    const {
      url, method,
      filters, redirect,
    } = paramsToRequest;
    if (!url || !method) {
      console.log(`${method}_REQUEST`.toUpperCase(), paramsToRequest);
      return false;
    }
    dispatch(ExampleActions.incrementLoader());
    axios(paramsToRequest).then((res) => {
      console.log(`[${paramsToRequest.method} OK]`.toUpperCase(), res);
      if (filters) {
        flat('', res.data, res.data);
        Object.keys(res.data)
          .filter(key => !filters.includes(key))
          .forEach(key => delete res.data[key]);
      }
      dispatch({
        type: ExampleActions[`${method}_REQUEST`.toUpperCase()],
        payload: res.data,
      });
      dispatch(ExampleActions.decrementLoader());
      if (redirect) {
        dispatch(push(redirect));
      }
    }).catch((err) => {
      console.log(`[${method.toUpperCase()} ERROR]`.toUpperCase(), err);
      dispatch(ExampleActions.updateError(err));
      dispatch(ExampleActions.resetLoader());
    });
    return true;
  }

  // Action set asyncData to Firebase
  static addColection = project => (dispatch, getState, { getFirestore }) => {
    dispatch(ExampleActions.incrementLoader());
    const firestore = getFirestore();

    firestore.collection('projects').add(project).then(() => {
      dispatch(ExampleActions.decrementLoader());
      dispatch({
        type: ExampleActions.CREATE_COLECTION,
        payload: project,
      });
    }).catch((err) => {
      dispatch(ExampleActions.updateError(err));
      dispatch(ExampleActions.resetLoader());
    });
  }
}
